import React from 'react'
import { Navbar } from 'react-bootstrap'
import { Link } from 'react-browser-router'

// get a nav bar item for each entity leading to the corresponding management page
function getLinks(entities){
  let list = []

  // eslint-disable-next-line no-unused-vars
  for(let entity in entities){
    const link = entities[entity].link, base = entity !== 'Αναζήτηση' ? "/manage" : ""

    if(entities[entity].isSeparator)
      list.push(<Navbar id="separator" key="separator"><div></div></Navbar>)
    list.push(<Navbar key={entity}> 
          <Link to={base + link.path} className="nav-link" onClick={onClick}>
              {entity}<i className={link.icon}></i>
          </Link> 
        </Navbar>)

  }
  return list
}

// remove underline decoration from all links and add it to the one clicked
function onClick(e){
  document.querySelectorAll(".nav-link").forEach(c => c.style.textDecoration = "none")
  e.target.style.textDecoration = "underline"
}

export { getLinks }
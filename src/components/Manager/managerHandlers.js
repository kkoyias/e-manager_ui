import { buttonMap, showError } from './managerMethods'
import { handleEditing } from "./managerUtils";
import { toggleSortingIcon, orderMap, resetIcon } from '../Panel/panelUtils'

// on overview clicked adjust panel & state
function overviewHandler(e, item){
    e.preventDefault()
    const {isInserting, selectedRecord, ...prevState} = this.state
    const state = { isInserting: false, selectedRecord: false } 
    this.setState(state)
    this.fetchData({...prevState, ...state}, {id: item.id,name: item.name}, true)
                        
}

// on page size change adjust panel & state
function panelSizeHandler(e){
    e.preventDefault()
    const { loading, size, isInserting, selectedRecord, ...prevState } = this.state
    const state = {loading: true, size: e.target.firstChild.value, isInserting: false, selectedRecord: false}
    this.setState(state)
    this.fetchData({...prevState,...state})
}

// on sorting icon click, toggle it's direction and request data in reverse order
function directionHandler(event){

    // get previous sorting field & order from state
    let { sort, ...prevState } = this.state
    const separatorIndex = sort.indexOf(','), field = sort.slice(0, separatorIndex)
    const order = sort.slice(separatorIndex + 1)

    // define new state changing just the ordering direction
    const state = {sort: field + ',' + orderMap[order]}
    this.setState(state)

    // change direction of icon
    let sortingIcon = document.querySelector("i[data-key=" + event.target.dataset.key + "]")
    toggleSortingIcon(sortingIcon, orderMap[order])

    // retrieve data in the requesting order
    this.fetchData({...prevState, ...state})
}

// once a certain field from the header is picked, hide the previously displayed icon
// display one next to the selected field & render the panel ordered by it
function sortingHandler(event){

    // find out which field was chosen to be used for sorting
    const { key } = event.target.dataset, header = document.querySelector("th[data-key=" + key + "]")
    
    // get previous sorting field, if any & reset the corresponding icon
    const resetIconBounded = resetIcon.bind(this)
    
    let state = {}

    // if this header was already selected, reset sorting to sorting by id
    if(resetIconBounded() === header.dataset.key)
        state = {sort: "id"}
    // else make sorting icon of the newly selected header visible
    else{ 
        const sortIcon = header.lastChild, { order } = sortIcon.dataset
        sortIcon.style.visibility = "visible"       
        state = {sort: key + ',' + orderMap[order]}
    }

    // sort table based on the column selected using the given order
    this.setState(state)
    this.fetchData({...this.state, ...state})
}

// on input change, update state to reflect the input content
function inputHandler(event){
    const { index, key } = event.target.dataset, { data } = this.state
    data[index][key] = event.target.value
    if(document.querySelectorAll("input[readonly]").length < document.querySelectorAll("input").length)
        this.setState({data: data})
}

// on page change cancel any insertions, set state.page to the selected one and fetch it
function pageHandler(index){
    const {page, isInserting, selectedRecord, ...prevState} = this.state
    const state = {page: index, isInserting: false, selectedRecord: false}
    this.setState(state)
    this.fetchData({...prevState, ...state}) // merge state changes with previous state
}

// hide an alert box
function alertHandler(index){
    console.log(index)
    const {alerts} = this.state
    alerts.splice(index, 1)
    this.setState({alerts : alerts})
}

const staticButtons = {
    'Ακύρωση' : { handler: cancelRecord, icon: "fas fa-undo-alt" },
    'Επεξεργασία' : { handler: handleEditing },
    'Επαναφορά' : { handler: handleEditing, icon: "fas fa-undo-alt" }
}

/* Handle all CRUD operations on button click */

// on a button press, send the appropriate Http request to our API to POST, PATCH or DELETE a record 
function buttonHandler(event, id){

    // adjust target info based on whether the actual button or the icon inside it was clicked
    let target = {}
    if(event.target.innerText) 
        target = {text: event.target.innerText, id: event.target.id}
    else
        target = {text: event.target.dataset.text, id: event.target.dataset.parent}
        
    const operation = target.text, handler = crudRecord.bind(this)
    
    // if this a static - frontEnd only operation, call the appropriate method
    if(operation in buttonMap === false){
        const method = staticButtons[operation].handler.bind(this)
        method(target)
    }
    // else if all required fields are filled & input is valid, then perform a CRUD operation 
    else if(buttonMap[operation].validator){
        const validator = buttonMap[operation].validator.bind(this)
        
        if(validator(id))
            handler(operation, id)
    }
    // else perform a CRUD operation anyway(e.g a delete)
    else
        handler(operation, id)
}

// cancel a record before submitting, so remove last entry of the table
function cancelRecord(){
    this.setState({data: this.state.data.slice(0, this.state.data.length-1), isInserting: false})
}

// perform a record operation after getting confirmation
function crudRecord(operation, id){
    const { api, entity } = this.state, confirm = window.confirm(operation + " εγγραφής;"), info = buttonMap[operation]
    const body = info.body && info.body.bind(this)
    console.log(info.endPoint(api, entity, id))
    
    // ask for confirmation before any operation
    if(confirm){
        fetch(info.endPoint(api, entity, id),{
            method: info.method,
            headers: new Headers({'Content-Type' : 'application/json'}),
            body: body && JSON.stringify(body(id))
        })

        // set state based on the response
        .then(httpResponse => {
            const status = httpResponse.status
            const handler = info.responseHandler[status] && info.responseHandler[status].bind(this)

            // if this response was unexpected, display an error message
            if(!handler)
                this.setState({alerts: [{message: showError(info.declarative), type: "danger"}]})
            // else handle it
            else
                handler(id)
        })

        // or display an error message if something goes wrong, e.g failed to even get a response
        .catch(error => {
            console.error(error)
            this.setState({
                alerts: [{
                    message: "Παρουσιάστηκε σφάλμα. Παρακαλώ ελέγξτε τη σύνδεση σας στο διαδίκτυο.", 
                    type: "danger"
                }]
            })
        })
    }
}

export { inputHandler, pageHandler, buttonHandler, alertHandler, overviewHandler, 
         sortingHandler, directionHandler, panelSizeHandler, staticButtons  }
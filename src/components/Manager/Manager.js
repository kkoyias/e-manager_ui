import React from 'react'
import ReactDOM from 'react-dom'
import { Container, Spinner } from 'react-bootstrap'

import Panel from '../Panel/Panel'
import { TopHeader, MainHeader } from './managerHeaders'
import fetchData from './managerFetch'
import AlertBox from '../AlertBox/AlertBox'

import { getState } from './managerUtils'
import { insertRecord } from './managerMethods'
import { getUrlParams, serviceHeader } from './serviceManager'
import { inputHandler, pageHandler, buttonHandler, alertHandler, 
         sortingHandler, directionHandler, panelSizeHandler, overviewHandler } from './managerHandlers'

import './manager.css'

class Manager extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            ...getState(props),
            sort: "id",
            size: 4,
            api: props.api
        }
        this.fetchData = fetchData.bind(this)
        this.pageHandler = pageHandler.bind(this)
        this.insertRecord = insertRecord.bind(this)
        this.inputHandler = inputHandler.bind(this)
        this.buttonHandler = buttonHandler.bind(this)
        this.alertHandler = alertHandler.bind(this)
        this.sortingHandler = sortingHandler.bind(this)
        this.directionHandler = directionHandler.bind(this)
        this.getState = getState.bind(this)
        this.getUrlParams = getUrlParams.bind(this)
        this.serviceHeader = serviceHeader.bind(this)
        this.panelSizeHandler = panelSizeHandler.bind(this)
        this.overviewHandler = overviewHandler.bind(this)
        this.Panel = Panel.bind(this)
        this.TopHeader = TopHeader.bind(this)
        this.MainHeader = MainHeader.bind(this)
    }

    // on mounting, fetch page 0 of this entity, set loading & error values appropriately
    componentDidMount(){
        ReactDOM.findDOMNode(this).addEventListener('click', () => this.setState({alerts: []}))
        this.fetchData(this.state)
    }

    // on each update, if the entity url parameter has changed, fetch page 0 of the new entity
    UNSAFE_componentWillReceiveProps(props){

        const state = {...this.getState(props), urlParams: this.state.urlParams}
        this.setState(state)
        
        // use the new state because setState is asynchronous
        this.fetchData({...this.state, ...state})
    }

    render(){
        const { parent, alerts, alertHandler, loading, searchMode } = this.state

        return (
            <Container>
                { parent && this.serviceHeader(parent)}
                <this.TopHeader/>
                <this.MainHeader searchMode={searchMode}/>

                {/* while loading display a spinning loader, once data is here display a table*/}
                {loading ? 
                    <Spinner id="loading-spinner" animation="border"/> 
                        : 
                    <div>
                        <this.Panel />
                        <AlertBox alerts={alerts} handler={alertHandler}/>
                    </div>
                }
            </Container>
        )
    }
}

export default Manager

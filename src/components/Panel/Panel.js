import React from 'react'
import { Table, Container } from 'react-bootstrap'
import { getRow, getHeader } from './panelConstructor'
import Paginate from './Paginate'
import './panel.css'

function Panel(){

    const { pageHandler } = this, { data, meta } = this.state, paginateProps = {meta, pageHandler}
    const getHeaderBounded = getHeader.bind(this)

    return(
        <Container>
            <Table striped bordered hover responsive id="panel">
                <thead className="thead-dark">
                    <tr>
                        {getHeaderBounded()}
                        
                        {/* add two fields for the upcoming buttons*/
                        data && data.length > 0 && [<th key="0"></th>, <th key="1"></th>]}
                    </tr>
                </thead>
                <tbody>
                    {data && data.map(getRow, this)} 
                </tbody>
            </Table>
            <div className="paginate">
                {meta && meta.totalPages > 1 && <Paginate {...paginateProps}/>}
            </div>
        </Container>)
}

export default Panel
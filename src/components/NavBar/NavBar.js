import React from 'react'
import { Nav, Navbar } from 'react-bootstrap'
import { Link } from 'react-browser-router'
import { getLinks } from './navBarMethods' 
import entities from '../configuration/entities'
import './navBar.css'

// help user navigate throughout e-bid using a navigation bar 
function NavBar() {

  return (
    <Nav className="navbar navbar-expand-md navbar-dark bg-dark">

      {/* display a home icon leading to homepage */}
      <Navbar.Brand className="logo mr-auto">
        <Link to="/" onClick={() => document.querySelectorAll(".nav-link").forEach(i => i.style.textDecoration = "none")}>
          <i className="fa fa-lg fa-home"></i>
        </Link>
      </Navbar.Brand>

      {/*help user navigate throughout this application */}
      <Navbar>
        {getLinks(entities)}
      </Navbar>
    </Nav>
  )
}

export default NavBar
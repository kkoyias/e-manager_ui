import React from 'react'
import ReactDOM from 'react-dom'
import entities from '../configuration/entities'

import { fetchAllRecords } from '../Utils/functions'
import { createInputSet, extractFieldSet } from './formUtils'

class MainForm extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            api: "http://localhost:8080/api",
            loading: true,
            category: this.props.category,
            alert: null,

            // get all fields of this category as a list of key-value pairs
            fields: extractFieldSet(props.category)
        }
        this.fetchAllRecords = fetchAllRecords.bind(this)
        this.fetchData = this.fetchData.bind(this)
        this.createInputSet = createInputSet.bind(this)
        this.updateFieldsData = this.updateFieldsData.bind(this)
        this.handleFetchError = this.handleFetchError.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    componentDidMount(){
        ReactDOM.findDOMNode(this).addEventListener('click', () => this.setState({alert: null}))
        this.fetchData(this.state)
    }

    // on each category switch, fetch again
    UNSAFE_componentWillReceiveProps(props){

        if(props.category !== this.state.category){
            const state = {
                loading: true,
                category: props.category,
                fields: extractFieldSet(props.category)
            }
            this.setState(state)

            // make sure to fetch passing the updated state because of setState's asynchronous nature
            this.fetchData(state)
        }
    }

    async fetchData(state){
        const args = {api: this.state.api, handleSuccess: this.updateFieldsData, handleError: this.handleFetchError}
        const fetchFunction = (f => f.isEntity && this.fetchAllRecords({field: f.label, ...args}))
        await state.fields.forEach(fetchFunction)
        this.setState({loading: false})
    }

    updateFieldsData(jsonResponse, label, lastFetch){
        let fields = this.state.fields, index = fields.findIndex(f => f.label === label)
        fields[index].data = jsonResponse

        // update state & set loading to false if this was the last fetch and loading was true before
        this.setState({fields, loading: this.state.loading && !lastFetch})
    }

    handleFetchError(error){
        this.props.onError({message: "Αποτυχία ανάκτησης δεδομένων. Παρακαλώ προσπαθήστε ξανά αργότερα", type: "danger"}) 
        console.error(error)
    }

    onSubmit(e){
        e.preventDefault()
        const { fields } = this.state

        // if no field was filled, throw an error message
        if(!fields.some(f => f.value))
            this.setState({alert: "Παρακαλώ συμπληρώστε τουλάχιστον ένα πεδίο."})
        else
            this.props.onSubmit(fields)
    }

    render(){
        const { loading, fields, category, alert } = this.state

        return(
            <form onSubmit={this.onSubmit} className="form-group">
                <h4 id="category-prompt">
                    {"Παρακαλώ συμπληρώστε τη φόρμα της κατηγορίας: "}<strong>{category}</strong>
                </h4>
                <i id="category-icon" className={entities[category].link.icon + " fa-5x"}></i>
                {loading || this.createInputSet(fields)}
                <h6 id="search-alert" style={{visibility : alert ? "visible" : "hidden"}}>{alert || "Placeholder"}</h6>
                <button className="btn btn-outline-primary" type="submit">Αναζήτηση</button>
            </form>
        )
    }

}

export default MainForm
import { twoWayMap } from '../Utils/functions'

// define sorting types and properties
const initialSortingIcon = {"data-order" : "desc", className: "fa fa-sort-down"}
const iconMap = twoWayMap({up : "down"}), orderMap = twoWayMap({asc : "desc"})
const orderToIcon = {asc : "up", desc : "down"}

// toggle an icon's direction and set order to reverse
function toggleSortingIcon(icon, order) {
    icon.className = "fa fa-sort-" + iconMap[orderToIcon[order]] 
    icon.dataset.order = orderMap[order]
}

function resetIcon(icon){
    const { sort } = this.state, index = sort.indexOf(',')
    let prevHeader = null
    if(index > 0){

        prevHeader = document.querySelector("th[data-key=" + sort.substr(0, index) + "]")
        // reset properties of the previous sorting icon, if any
        if(prevHeader){
            let icon = prevHeader.lastChild
            icon.className = initialSortingIcon.className
            icon.dataset.order = initialSortingIcon["data-order"]
            icon.style.visibility = "hidden"
        }
    }
    return prevHeader && prevHeader.dataset && prevHeader.dataset.key
}

// given an object and a list of selected properties
// concatenate their values as a string and separate them with a space
function getKeyValuesAsString(item, selectedKeys){
    let rv = []

    // eslint-disable-next-line no-unused-vars
    for(let key in item){
        if(selectedKeys.indexOf(key) !== -1)
            rv.push(item[key])
    }
    return rv.join(' ')
}


export { getKeyValuesAsString, toggleSortingIcon, orderMap, initialSortingIcon, resetIcon }
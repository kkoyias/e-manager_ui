import React from 'react'
import './App.css'

function Home(){
    return(
        <div id="home" className="display-4">
            <h1>Αρχική σελίδα διαχείρισης και διανομής τεχνικού εξοπλισμού</h1>
            <h2>Ίδρυμα της Βουλής των Ελλήνων</h2>
        </div>
    )
}

export default Home
import React from 'react'
import entities from '../configuration/entities'
import keyToEntity from '../configuration/keyToEntity'
import toPlural from '../configuration/toPlural'

function extractOptions(item, attribute){
    const entity = entities[keyToEntity[toPlural[attribute]]]
    const keys = Object.keys(entity.fields).filter(f => entity.fields[f].isReferenced)
    let rv = []

    // eslint-disable-next-line no-unused-vars
    for(let key in item){
        if(keys.indexOf(key) !== -1)
            rv.push(item[key])
    }
    return rv.join(' ')
}

// find all options that include input as a substring, put those that match prefix first & color matches
function findMatches(list, input){

    if(input){
        let matchPrefix = [], includesOption = []
        const lowerInput = input.trim().toLowerCase()

        // eslint-disable-next-line no-unused-vars
        for(let option of list){
            const lowerOption = option.trim().toLowerCase()
            const index = lowerOption.search(lowerInput)

            // if input is prefix of an option
            if(index === 0)
                matchPrefix.push(
                    <span value={option}>
                        <span className="matching-str">
                            {option.slice(0, input.length)}
                        </span>
                        {option.slice(input.length)}
                    </span>
                )

            // else if an option contains input as a substring
            else if(index > 0)
                includesOption.push(
                    <span value={option}>
                        {option.slice(0, index)}
                        <span className="matching-str">{option.slice(index, index + input.length)}</span>
                        {option.slice(index + input.length)}
                    </span>
                )
        }
        return matchPrefix.concat(includesOption)
    }
    else 
        return list.map(i => <span value={i}>{i}</span>)
}

export { extractOptions, findMatches }

import React from 'react'
import ReactDOM from 'react-dom'
import { Jumbotron, Container } from 'react-bootstrap'

import Manager from '../Manager/Manager'
import entities from '../configuration/entities'
import AlertBox from '../AlertBox/AlertBox'
import MainInput from './MainInput'
import MainForm from './MainForm'
import { createBody } from './formUtils'
import './search.css'

class Search extends React.Component{
    constructor(props){
        super(props)
        this.state = {category: null, alerts: [], api: props.api}
        this.onTop = this.onTop.bind(this)
    }

    componentDidMount(){
        ReactDOM.findDOMNode(this).addEventListener('click', () => {
            if(this.state.alerts.length > 0)
                this.setState({alerts: []})
        })
    }

    // scroll to top & focus on the main input field
    onTop(){
        window.scrollTo(0,0)
        document.getElementById("search-main-input").focus()
    }

    render(){
        const { category, query, alerts, api } = this.state
        const entity = query && entities[category].link.path.slice(1)
        const managerProps = {api, entity, query}

        return (
            <div id="search-top">
                <Jumbotron>
                    <h1 className="display-4">{"Αναζήτηση "}<i id="search-icon" className="fa fa-search"></i></h1>
                    <MainInput createForm={(category) => this.setState({category: category, query: null})}/>
                </Jumbotron>
                {category && 
                    <React.Fragment>
                        {query ? 
                            <React.Fragment>
                                {<Manager {...managerProps}/>}
                                <h6 id="search-back-to-top" onClick={this.onTop}>
                                Πίσω στην κορυφή <i className="fas fa-arrow-alt-circle-up"></i>
                                </h6>
                            </React.Fragment>
                                :
                            <Container>
                                <MainForm 
                                    category={category} 
                                    onSubmit={(payload) => this.setState({query: createBody(payload, category)})}
                                    onError={(alert) => this.setState({alerts: [alert]})}
                                />
                            </Container>
                        }
                        {alerts && <AlertBox {...this}/>}
                    </React.Fragment>
                }
            </div>
        )
    }
}

export default Search
import React from 'react'

import { extractOptions, findMatches } from './autoCompleteMethods'
import './auto-complete.css'

const maxSuggestions = 5
class AutoComplete extends React.Component {
    constructor(props) {
        super(props)
        const options = props.options && props.options.map ? 
            props.options.map(item => extractOptions(item, props.input["data-key"])) : []
        
        this.state = {  
            data: options,
            input: props.input,
            suggestions: props.input.value === "" && props.input.focus ? 
                options.map(i => <span value={i}>{i}</span>) : [],
            selected: null 
        }
        this.onChange = this.onChange.bind(this)
    }

    componentDidMount(){

        const { input } = this.state
        const element = document.querySelectorAll("input[data-key=" + input["data-key"] + "]")[input["data-index"]]

        if(element){
            // display all suggestions on focus
            element.addEventListener('focus', () => {
                if(!input.value)
                    this.setState({suggestions: this.state.data.map(i => <span value={i}>{i}</span>)})
            })
            // hide all suggestion when focus is lost
            element.addEventListener('blur', () => this.setState({suggestions: []}))            
        }
    }

    UNSAFE_componentWillReceiveProps(props){
        this.setState({input: props.input, options: props.options})
    }

    // suggest records while typing using regular expression matches
    onChange = (e) => {
        const input = e.target.value, { onChange } = this.state.input
        let suggestions = []

        if(!e.target.readOnly)
            suggestions = findMatches(this.state.data.sort(), input).slice(0, maxSuggestions)
        this.setState(() => ({ suggestions }))
        onChange(e)
    }

    // return suggestions as a dropdown list
    returnResults() {
        const suggestions = this.state.suggestions, { input } = this.state, { onChange } = input

        return(
            suggestions.length > 0 && 
            <ul> 
                {suggestions.map((item, index) => 
                    <li
                        data-index= {input["data-index"]}
                        data-key={input["data-key"]}
                        key={index} 
                        onMouseDown={() => {
                            this.setState({suggestions: []})
                            onChange({
                                target: {
                                    value: item.props.value, 
                                    dataset: {
                                        key: input["data-key"], 
                                        index: input["data-index"]
                                    }
                                }
                            })
                        }}>
                    {item}
                    </li>)} 
            </ul>)
    }

    render() {
        const list = this.returnResults(), {onChange, ...input} = this.state.input  

        return(
            <div className="auto-results">
                <input 
                    {...input}
                    onChange={this.onChange} 
                />
                {list}
            </div>)
    }
}

export default AutoComplete
import React from 'react'

function prevServiceHandler(parent){
    const {isInserting, ...prevState} = this.state
    const state = {isInserting: false, selectedRecord: false, parent: prevState.parent.parent}
    this.setState(state)
    this.fetchData({...prevState, ...state}, parent.parent, false)
}

function serviceHeader(parent){
    const handler = prevServiceHandler.bind(this)

    return (<h1 id="back-to-parent" className={"service-link"}>
                        <p className={"service-link"} 
                            onClick ={() => handler(parent)}>
                            {parent &&
                            <div>
                                <i className="fa fa-arrow-circle-left"></i>
                                <span>{parent.parent ? parent.parent.name : "Πίσω"}</span>
                            </div>}
                        </p> 
            </h1>)
}

function getUrlParams(params, parent, forward){
    let urlParams = ""

    // if this is a forward step set parent to this(moving down, one level below)
    if(parent){
        const currentParent = forward ? {...parent, parent: this.state.parent} : this.state.parent.parent
        this.setState({parent: currentParent, selectedRecord : false})
        urlParams = getParams(parent, params).asString
    }

    // if there is no parent specified
    else if(forward)
        urlParams = getParams(this.state.parent, params).asString
    else{
        this.setState({parent: null})
        urlParams = getParams(parent, params).asString
    }
    return urlParams
}

// given the url parameters of our location return those that matter
function getParams(params, neededParams){
    let rv=""

    // eslint-disable-next-line no-unused-vars
    for(let key in params){
        if(neededParams && Object.keys(neededParams).includes(key))
            rv += '&' + key + '=' + params[key] 
    }
    return {asString: rv, asObject: params}
}

export { getUrlParams, serviceHeader }
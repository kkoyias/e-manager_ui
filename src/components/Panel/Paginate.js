import React from 'react'
import ReactPaginate from 'react-paginate'

function Paginate(props){

    const { meta, pageHandler } = props

    return(
        <div className = "div-center">
            <ReactPaginate
                pageCount = {meta ? meta.totalPages : 0}
                pageRangeDisplayed = {5}
                marginPagesDisplayed = {5}
                forcePage={meta ? meta.number : 0}
                onPageChange = {pageHandler ? ((e) => pageHandler(e.selected)) : undefined}
                nextLabel = "Επόμενη"
                previousLabel = "Προηγούμενη"
                containerClassName = "list-group list-group-horizontal paginate"
                pageClassName = "list-group-item"
                activeClassName = "list-group-item list-group-item-primary" 
                previousClassName = "list-group-item"
                breakClassName = "list-group-item" // the ellipsis
                nextClassName = "list-group-item"
            />
        </div>)
}

export default Paginate
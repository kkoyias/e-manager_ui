import React from 'react'
import AutoComplete from '../AutoComplete/AutoComplete'

import propToGreek from '../configuration/propToGreek'
import keyToEntity from '../configuration/keyToEntity'
import entities from '../configuration/entities'
import toPlural from '../configuration/toPlural'

import { buttonMap } from '../Manager/managerMethods'
import { staticButtons} from '../Manager/managerHandlers'
import { getKeyValuesAsString, initialSortingIcon } from './panelUtils'
import { valueUntilCharacter } from '../Utils/functions'
 
function getHeader(){
    let rv = []
    const { sortingHandler, directionHandler } = this
    const { entity } = this.state, { fields } = this.state.attributes

    // eslint-disable-next-line no-unused-vars
    for(let key in fields){
        const canBeSorted = !entities[keyToEntity[entity]].fields[key].isEntity

        rv.push(<th key={key} data-key={key}>
                    <span 
                        data-key={key} 
                        data-sort={canBeSorted}
                        onClick={canBeSorted ? sortingHandler : undefined} 
                        className="panel-header" 
                        title={canBeSorted ? "Επιλέξτε ως πεδίο ταξινόμησης" : "Το πεδίο δεν υποστηρίζει ταξινόμηση"}>
                        {propToGreek[key] + (!fields[key].isOptional ? "  *" : '')}
                    </span>
                    {canBeSorted && <i data-key={key} {...initialSortingIcon} onClick={directionHandler}></i>}
                </th>)
    }
    return rv
}

// transform a record to a table row
function getRow(item, index){
    const getDataBounded = getData.bind(this)
    return <tr className="table-row" key={item.id || 0}>{getDataBounded(item, index)}</tr>
}

// given an item returned from an api, get all field values of it as table data 
function getData(item, index){
    let rv = []
    const mySubmitButton = submitButton.bind(this), {id} = item, { entity, attributes } = this.state

    // eslint-disable-next-line no-unused-vars
    for(let key in attributes.fields){

        let value = null
        
        if(key === "id" || key === "added")
            continue

        const meta = entities[keyToEntity[entity]], info = meta.fields[key]

        // get primitive types as they are
        if(!(item[key] instanceof Object))
            value = item[key] && (info.isEntity ? item[key] : valueUntilCharacter(item[key], ','))

        // get the value field of any non-primitive / complex field omitting fields like _links and _embedded
        else if(key[0] !== '_'){
            const fields = entities[keyToEntity[toPlural[key]]].fields
            value = getKeyValuesAsString(item[key], Object.keys(fields).filter(f => fields[f].isReferenced))
        }
        else
            continue

        const inputProps = attributes.fields[key].input
        const commonProps = inputProps && {
            ...inputProps,
            title: inputProps["data-fixed"] ? "Μη τροποποιήσιμο πεδίο" : null,
            readOnly: !item.added,
            "data-index": index,
            "data-key": key,
            value: value !== null ? value : (item.added ? "" : "-"),
            autoFocus: item.added && rv.length === 0, // focus on the 1st column of the item to be added
            className: "form-control for-item-" + id,
            onChange: (!inputProps.readOnly ? this.inputHandler : null)
        }
        rv.push(
            <td key={rv.length}>
                {!info.isEntity ?
                    <input {...commonProps}/>
                    :
                    <AutoComplete 
                        options={info.data} 
                        input={commonProps}
                />}
                {/* if this item is tree-structured, a field will contain an inner link */
                    info.hasLink && !item.added &&
                    <p id={"inner-service"}className="service-link" 

                        // on click, cancel all additions and editing & redirect to the requesting service's panel 
                        onClick={(e) => this.overviewHandler(e, item)}>
                        Επισκόπηση<i className="fa fa-eye"></i>
                    </p>
                }
            </td>)
    }

    // add buttons, distinguishing between empty and non-empty records ignoring the attribute 'added'
    rv.push(mySubmitButton(item.added ? "Προσθήκη" : "Επεξεργασία", "primary", rv.length, id))
    rv.push(mySubmitButton(item.added ? "Ακύρωση" : "Διαγραφή", "danger", rv.length, id))
    return rv
} 

function submitButton(content, type, key, id){

    // get the appropriate icon for this button
    const icon = (buttonMap[content] && buttonMap[content].icon) 
        || (staticButtons[content] && staticButtons[content].icon)

    return (<td className="button-cell" key={key}>
                <button 
                    type="submit"
                    id={"button-" + key + "-for-" + id}
                    onClick = {(e => this.buttonHandler(e, id))}
                    className={"btn btn-sm btn-outline-" + type + " btn-for-" + id}>
                        {content}
                        {<i id="button-icon" 
                            data-text = {content}
                            data-parent = {"button-" + key + "-for-" + id}
                            className={icon}>
                        </i>}
                </button>
            </td>)
}

export { getData, getRow, getHeader }
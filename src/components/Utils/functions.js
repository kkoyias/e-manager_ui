import entities from '../configuration/entities'
import keyToEntity from '../configuration/keyToEntity'
import toPlural from '../configuration/toPlural'

function fetchAllRecords(props){
    const path = entities[keyToEntity[toPlural[props.field]]].link.path
    return new Promise(async (resolve, reject) => {
        fetch(props.api + path + "/all")
        .then(httpResponse => httpResponse.json())

            // the handler of the latest response is supposed to set loading to false while updating data
            .then(jsonResponse => resolve(props.handleSuccess(jsonResponse, props.field)))
        .catch(error => reject(props.handleError(error)))
    })
}

function removeLinks(item){
    const {_links, _embedded, ...props} = item
    return props
}

function twoWayMap(map){
    
    // eslint-disable-next-line no-unused-vars
    for(let key in map){
        let value = map[key]
        if(value in map === false)
            map[value] = key
    }
    return map
}

function valueUntilCharacter(value, char){
    if(typeof value === "string" && value.indexOf(char) > 0)
        value = value.slice(0, value.indexOf(char))

    return value
}

export { fetchAllRecords, removeLinks, twoWayMap, valueUntilCharacter }
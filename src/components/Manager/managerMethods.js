import { haveSameValues } from './managerUtils'
import { resetIcon } from '../Panel/panelUtils'
import keyToEntity from '../configuration/keyToEntity'
import entities from '../configuration/entities'
import toPlural from '../configuration/toPlural'
import propToGreek from '../configuration/propToGreek'

// create a new input for the new record
function insertRecord(event){
    event.preventDefault()
    let { data, attributes, isInserting, selectedRecord } = this.state, record = {added: true} 

    // eslint-disable-next-line no-unused-vars
    for(let key in attributes.fields)
        record[key] = null

    // add if and only if previous input is empty
    if(isInserting)
        alert("Σφάλμα: Παρακαλώ πρώτα ολοκληρώστε ή ακυρώστε την προσθήκη της τελευταίας εγγραφής")
    else if(selectedRecord)
        alert("Σφάλμα: Παρακαλώ πρώτα συμπληρώστε ή επαναφέρετε την εγγραφή που επεξεργάζεστε")
    else{
         data.push(record)
         this.setState({data: data, isInserting: true})
     }
}

function constructField(values, keys){

    let i = 0, rv = {}
    for(i = 0; i < values.length; i++)
        rv[keys[i]] = values[i]
    return rv
}

// create an object to be sent into an http request
function constructRecord(record, fields){
    let recordDTO = {}

    // eslint-disable-next-line no-unused-vars
    for(let key in record){
        if(key === "id" || !fields[key])
            continue

        let value = record[key]

        // if attribute is an object and has been edited
        // then it has a string value so convert it back to an object
        if(value && fields[key].isEntity && !(value instanceof Object)){
            value = fields[key].hasSpaceSeparatedFields ? value.split(' ') : [value]
            value = constructField(value, Object.keys(entities[keyToEntity[toPlural[key]]].fields))
        }

        // if attribute is an object get it's id
        if(fields[key].isEntity && value){
            const objectFields = entities[keyToEntity[toPlural[key]]].fields
            const referencedFields = Object.keys(objectFields).filter(f => objectFields[f].isReferenced)
            const predefinedChoice = fields[key].data.find(item => haveSameValues(value, item, referencedFields))
            recordDTO[key + "ID"] = predefinedChoice ? predefinedChoice.id : 0 
        }

        // else just copy it's value
        else
            recordDTO[key] = record[key]
    } 
    return recordDTO
}

// get the appropriate body for a POST request
function addRecordBody(){
    const {added, ...obj} = this.state.data[this.state.data.length-1]
    const record = constructRecord(obj, this.state.attributes.fields)
    console.log(record)
    return record 
}

// get the appropriate body for a PUT request
function editRecordBody(id){
    const record = constructRecord(this.state.data.find(item => item.id === id), this.state.attributes.fields)
    console.log(record)
    return record
}

// define attributes & methods for each operation available
const buttonMap = {
    "Προσθήκη" : {
        endPoint: (api, entity) => api + '/' + entity,
        method: "POST",
        body: addRecordBody,
        declarative: "προσθήκης",
        responseHandler : {
            201: addSuccess,
            409: duplicateEntry
        },
        validator: validateInput,
        icon: "fa fa-plus-circle"
    },
    "Ανανέωση" : {
        endPoint: (api, entity, id) => api + '/' + entity + '/' + id,
        method: "PUT",
        body: editRecordBody,
        declarative: "ανανέωσης",
        responseHandler : {
            200 : editSuccess,
            409: duplicateEntry
        },
        validator: validateInput,
        icon: "fa fa-arrow-circle-up"
    },
    "Διαγραφή" : {
        endPoint: (api, entity, id) => api + '/' + entity + '/' + id,
        method: "DELETE",
        declarative: "διαγραφής",
        responseHandler :  {
            204: deleteSuccess
        },
        icon: "fas fa-trash"
    }
}

// decide whether a form - row was filled correctly and can be submitted 
function validateInput(id){
    const row = this.state.data.find(item => item.id === id), {fields} = this.state.attributes

    // eslint-disable-next-line no-unused-vars
    for(let key in fields){
        const field = fields[key]

        // if a field is left blank, then input is invalid
        if(!row[key] && !field.isOptional){
            this.setState({alerts: [{
                message: "Το πεδίο '" + propToGreek[key] + "' είναι υποχρεωτικό και/ή συμπληρωμένο λανθασμένα", 
                type: "warning"}
            ]})
            return false
        }

        const alertMessageForPredefined = {
            message: "Παρακαλώ επιλέξτε από τις προκαθορισμένες τιμές για τo πεδίo '" + propToGreek[key] + "'", 
            type: "warning"
        }

        // if a field requires a predefined value but a custom was given, then this input is invalid too
        if(field.isEntity){
            let value = row[key]
            const attributes = entities[keyToEntity[toPlural[key]]].fields
            const fieldsOfEntity = Object.keys(attributes).filter(f => attributes[f].isReferenced)

            // if attribute is an object and has been edited
            // then it has a string value so convert it back to an object
            if(!(value instanceof Object) && value){
                value = fields[key].hasSpaceSeparatedFields ? value.split(' ') : [value]
                value = constructField(value, Object.keys(entities[keyToEntity[toPlural[key]]].fields))
            }

            // if no object with this particular value exists in the predefined list
            // and this field is either mandatory or filled, display an error message
            if((!field.isOptional || value) && !field.data.find(item => haveSameValues(value, item, fieldsOfEntity))){
                this.setState({alerts: [alertMessageForPredefined]})
                return false
            }
        }
    }
    return true
}

const showError = (action => "Αδυναμία " + action + ". Παρακαλώ ελέγξτε τη σύνδεση σας στο διαδίκτυο.")

function duplicateEntry(){
    this.setState({alerts: [{message: "Προσοχή: Διπλότυπη εγγραφή", type: "warning"}]})
}

    /********  response handling ********/

// add record
function addSuccess(){

    // if panel was sorted by some field, reset so that the addition is placed in the last page
    const resetIconBounded = resetIcon.bind(this)
    resetIconBounded()

    const {alerts, isInserting, page, ...prevState} = this.state, { fields } = this.state.attributes
    const { size, totalPages, totalElements } = this.state.meta
    const state = {
        alerts: [{message: "Επιτυχής προσθήκη!", type: "success"}], 
        isInserting: false,
        sort: "id",
        page: "id" in fields ? 
            0 : totalElements % size ? totalPages-1 : totalPages // redirect to last page(0-indexed)
    }
    this.setState(state)
    this.fetchData({...prevState, ...state}, null, true)
}

// edit record
function editSuccess(id){

    // notify user of the successful update & reset buttons to initial state
    this.setState({alerts: [{message: "Επιτυχής ανανέωση!", type: "success"}]})
    this.buttonHandler({target: {innerText: 'Επαναφορά', id: "-" + id}})
    this.fetchData(this.state, null, true)
}

// delete record
// on successful record deletion move to previous page if this is a singe-record page
function deleteSuccess(){
    const { page, meta, ...prevState } = this.state, { size, totalElements } = meta

    const state = {
        alerts: [{message: "Επιτυχής διαγραφή!", type: "success"}],
        page: (totalElements - 1) % size ? page : page - 1,
        
    }
    this.setState(state)
    this.fetchData({...prevState, ...state}, null, true)
}

export { buttonMap, insertRecord, showError }
import React from 'react'
import { Alert } from 'react-bootstrap'
import './alert.css'

function AlertBox(props){
    
    const alerts = props.alerts && props.alerts.map((alert, index) => (
            <Alert key={index} className="alert-box" variant={alert.type} dismissible onClick = {() => props.handler(index)}>
                <strong>{alert.title}</strong>{alert.message} 
            </Alert>))

    return (<div>{alerts}</div>)
}

export default AlertBox
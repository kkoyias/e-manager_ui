import React from 'react'
import keyToEntity from '../configuration/keyToEntity'

function TopHeader(){
    const { entity, meta, api, size} = this.state

    return(
        <h1 id="export">
            {"Προβολή ως: "} 
            <a href={api + '/' + entity + "/all"} target="_blank" 
            rel="noopener noreferrer" download={entity + ".json"}>JSON</a>
                {/*" | "*/}
            {/*<a href={api + '/' + entity + "/all"} target="_blank" style={{textDecoration: "line-through"}}
            rel="noopener noreferrer" download={entity + ".xml"} disabled>XML</a><br/>*/}

            {/* allow user to adjust the number of rows of the panel if there is at least one entry */}
            {meta && meta.totalElements > 0 &&
            <div id="set-page">Μέγεθος σελίδας: 
                <form onSubmit={this.panelSizeHandler}> 
                    <input 
                        className="form-control"
                        type="number" 
                        min="1" max="100"
                        value={size} 
                        onChange={(e) => this.setState({size: e.target.value})}
                    />
                </form>
            </div>}
        </h1>
    )
}

function MainHeader(props){
    const { entity, attributes, parent } = this.state

    return(
        props.searchMode ? 
            <h1 id="category-prompt">{"Αποτελέσματα"}</h1>
                :
            <React.Fragment>
                <h2 
                    id="manager-title" 
                    className="display-4">
                    {attributes.isSection || "Χειρισμός oντότητας: "}
                    <strong>{keyToEntity[entity]}</strong>
                    {attributes.isFinal || 
                        <i id="add-icon" className="fa fa-plus-circle" title="Προσθήκη" onClick={this.insertRecord}></i>}
                </h2>
                {parent && <h3 id="note">{"Διεύθυνση-Τμήμα: "}<strong>{parent.name}</strong></h3>}
            </React.Fragment>
    )
}

export { TopHeader, MainHeader}
import { twoWayMap } from '../Utils/functions'
import { buttonMap } from './managerMethods'
import { staticButtons } from './managerHandlers'
import entities from '../configuration/entities'
import keyToEntity from '../configuration/keyToEntity'

// check whether two objects have same values on given properties
function haveSameValues(item, otherItem, props){
    if(!item || !otherItem)
        return false

    // eslint-disable-next-line no-unused-vars
    for(let i = 0; i < props.length; i++){
        // eslint-disable-next-line eqeqeq
        if((!(otherItem[props[i]] instanceof Object)) && item[props[i]] != otherItem[props[i]])
            return false
    }
    return true
}

// once the edit button is pressed enable editing
function handleEditing(target){

    // define buttonContent map
    const buttonContent = twoWayMap({'Επεξεργασία' : 'Ανανέωση', 'Επαναφορά' : 'Διαγραφή'})

    // get id of the record to be edited or reset 
    const {id, text} = target, recordId = parseInt(id.slice(id.lastIndexOf('-') + 1))

    // get input list for that record and for each of them toggle the readonly property
    const { selectedRecord, isInserting } = this.state
    const inputs = document.querySelectorAll("input.for-item-" + recordId)
    toggleReadOnlyAttribute(inputs, !(selectedRecord || isInserting))

    // if a record is being edit already then
    // notify the user that editing more than one records at a time is forbidden
    if(text === 'Επεξεργασία' && (selectedRecord || isInserting))        
        this.setState({alerts: [{
            message: "Παρακαλώ ολοκληρώστε ή επαναφέρετε την τρέχουσα επεξεργασία πριν την επόμενη",
            type: "warning"}
        ]})
    else{

        // adjust content for both buttons of this record
        document.querySelectorAll("button.btn-for-" + recordId)
            .forEach(btn => {
                let text = buttonContent[btn.innerText], lastChild = btn.lastChild
                let icon = (buttonMap[text] && buttonMap[text].icon) 
                    || (staticButtons[text] && staticButtons[text].icon)
                lastChild.className = icon
                lastChild.dataset.text = text
                btn.innerText = text
                btn.appendChild(lastChild)
            })
        
        // if reset is requested, put back previous content and enable editing other fields 
        if(text === 'Επαναφορά')
            this.setState({selectedRecord: false, data: putBack(selectedRecord, this.state.data)})
        // else this is an edit, store content temporarily, being able to reset it
        else
            this.setState({selectedRecord: Object.assign({}, this.state.data.find(item => item.id === recordId))})
    }
}

// replace a list item with another if these two have the same id
function putBack(item, list){
    let rv = list
    rv[list.findIndex(i => i.id === item.id)] = item 
    return rv
}

// toggle readonly attribute, setting it as true 
// if it is both false and no record to be edited has been selected
function toggleReadOnlyAttribute(list, disableReadOnly){
    for(let i = 0; i < list.length; i++)
        list[i].readOnly = !(disableReadOnly && list[i].readOnly) || list[i].dataset.fixed
}

function getState(p){
    const searchMode = !p.match , entity = searchMode ? p.entity : p.match.params.entity
        return {
            entity: entity, 
            loading: true, 
            alerts: entities[keyToEntity[entity]].info && [{
                title: "Σημείωση: ",
                message: entities[keyToEntity[entity]].info,
                type: "info"
            }],
            page: 0,
            attributes: entities[keyToEntity[entity]],
            selectedRecord: false,
            isInserting: false,
            parent: null,

            searchMode : searchMode,
            searchBody: p.query
}}


export { handleEditing, haveSameValues, getState }
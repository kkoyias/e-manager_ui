import { fetchAllRecords, removeLinks } from '../Utils/functions'

// get all predefined options for a certain field
async function fetchPredefined(state){
    const fetchAllRecordsBounded = fetchAllRecords.bind(this)
    const handleSuccess = updateFieldsData.bind(this), handleError = handleFetchError.bind(this)

    return new Promise(async (resolve, reject) => {
        // eslint-disable-next-line no-unused-vars
        for(let field in state.attributes.fields){
            if(state.attributes.fields[field].isEntity){

                // wait for all predefined choices to be loaded for this field
                await fetchAllRecordsBounded({field: field, api: this.state.api, 
                    handleSuccess: handleSuccess, handleError: handleError})
            }
        }
        // after all predefined choices are fetched, resolve the promise
        resolve("ok")
    })
}

// get the record list for a given entity by page
async function fetchData(state, parent, forward){
    const fetchPredefinedBounded = fetchPredefined.bind(this)
    const makeRequestBounded = makeRequest.bind(this)

    // first load all options for fields with predefined values(e.g building name for a service)
    if(!parent)
        await fetchPredefinedBounded(state)

    const { path, innerPath, params } = state.attributes.link, { api, searchMode } = this.state
    const { page, sort, size, entity } = state, fixedParams = "?page=" + page + "&sort=" + sort + "&size=" + size
    const urlParams = (!parent && !forward ? "" : this.getUrlParams(params, parent, forward))
    const url = api + path + getInnerPath(searchMode, innerPath) + fixedParams + urlParams
    console.log(url, state.parent)

    makeRequestBounded(url, entity)
}

// distinguish request between custom GET request & /search POST type request(those require a body)
function makeRequest(url, entity){
    const handleError = handleFetchError.bind(this), getBody = requestBody.bind(this)

    fetch(url, getBody() || null)
    .then(response => response.json())
        .then(data => this.setState({
            loading: false, 
            data: this.state.searchMode ? data.content : data["_embedded"][entity].map(removeLinks), 
            meta: data.page
    }))
    .catch(error => handleError(error))
}

// create body of a POST request
function requestBody(){
    const { searchMode, searchBody } = this.state
    return searchMode && {
        method: 'POST', 
        headers: {'Content-Type' : 'application/json'},
        body: JSON.stringify(searchBody)
    }
}


// once all predefined options for a certain field are fetched, update state
function updateFieldsData(jsonResponse, field){
    let attributes = this.state.attributes
    attributes.fields[field].data = jsonResponse
    this.setState({attributes})
}

// pop-up a danger alert box in case an error occurs while fetching data
function handleFetchError(error){
    this.setState({
        loading: false, 
        alerts: [{message: "Αποτυχία ανάκτησης δεδομένων. Παρακαλώ προσπαθήστε ξανά αργότερα", type: "danger"}]
    })
    console.error(error)
}

// extend request uri based on the mode & any possible path extensions
function getInnerPath(searchMode, innerPath){
    if(searchMode)
        return "/search"
    else 
        return innerPath || ""
}

export default fetchData

import React from 'react'
import { Row,Col } from 'react-bootstrap'
import { extractOptions } from '../AutoComplete/autoCompleteMethods'

import entities from '../configuration/entities'
import keyToEntity from '../configuration/keyToEntity';
import toPlural from '../configuration/toPlural'
import propToGreek from '../configuration/propToGreek'
import './search.css'

function extractSingleLayer(entity){
    // eslint-disable-next-line no-new-object
    return Object.entries(entities[entity].fields).map(entry => new Object({label: entry[0], ...entry[1]}))
}

function extractFieldSet(entity){
    
    // get a list of all primitive fields and their meta-data if not requested to be omitted on search
    let list = extractSingleLayer(entity).filter(field => !field.omitOnSearch)
    const fieldToEntity = (f) => keyToEntity[toPlural[f]]
    let updated = list.filter(f => f.isEntity) 

    // while the end result is growing do
    while(updated.length > 0){
        let temp  = []

        // for each field of those to be examined, that is an entity
        // eslint-disable-next-line no-unused-vars
        for(let field of updated){
            const alreadyReportedFields = list.map(item => fieldToEntity(item.label))
            const isNotReported = (f => alreadyReportedFields.indexOf(fieldToEntity(f.label)) < 0)

            // add it to the input list and insert the non-primitive fields of it to be examined
            const key = fieldToEntity(field.label)
            const extraFields = extractSingleLayer(key).filter(f => f.isEntity && f.isExtra && isNotReported(f))
            extraFields.forEach(f => temp.push(f))
        }
        list = list.concat(temp)
        updated = temp
    }

    return list
}

function reflectItem(item, label){
    return {id: item.id, field: label, text: extractOptions(item, label)}
}

function createSingleInput(field, index, colSpan){

    // create option list for this field
    const options = field.isEntity && field.data && field.data.map((i) => reflectItem(i, field.label))

    // define common properties for all input fields
    const commonProps = {
        className: "form-control",
        onChange: (e) => {
            let { fields } = this.state, { value } = e.target
            const indexOfUpdatedField = fields.findIndex(f => f.label === field.label)
            fields[indexOfUpdatedField].value = value
            this.setState({fields})
        }
    }
    const inputProps = {
        ...commonProps,
        ...field.input,
        value: field.value
    }

    return (
        <Col id="search-field" key={index} className={"col col-md-" + colSpan}>
            <label><strong>{propToGreek[field.label]}</strong></label>
            
            {/* discriminate between fields with predefined values & custom ones */
            field.isEntity ?
                <select {...commonProps}>
                    <option selected disabled hidden>{inputProps.placeholder}</option>
                    {options && options.map((o, i) => <option key={i} value={o.id}>{o.text}</option>)}
                </select>
                :
                React.createElement("input", inputProps)}
        </Col>
    )

}

function createInputSet(fields){
    const createSingleInputBounded = createSingleInput.bind(this)
    const gridCols = 12, fieldsPerRow = 2, colSpan = Math.floor(gridCols/fieldsPerRow)

    let rv = []
    for(let i = 0; i < fields.length; i += fieldsPerRow)
        rv.push(<Row key={i}>
                    {fields.slice(i, i + fieldsPerRow).map((f, i) => createSingleInputBounded(f, i, colSpan))}
                </Row>)
    return rv
}

// create payload for a search http request to be made
function createBody(payload, entity){
    const fields = entities[entity].fields
    let body = {simple: {}, complex: {}}

    // distinguish between own and foreign fields for a certain entity
    payload.filter(i => i.value).forEach(i => {

        // if this is some foreign field, include the id only adding the appropriate suffix
        if(i.label in fields === false)
            body.complex[i.label + "ID"] = parseInt(i.value)

        // else if this is a foreign key, convert to an integer
        else if(i.isEntity)
            body.simple[i.label] = parseInt(i.value)

        // else this is a simple key so send it in the appropriate format
        else
            body.simple[i.label] = fields[i.label].input.type === "number" ? parseInt(i.value) : i.value

    })
    return body
}

export { createInputSet, extractFieldSet, createBody }
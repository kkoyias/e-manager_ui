import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Switch, Route } from 'react-router-dom'
import NavBar from './NavBar/NavBar'
import Home from './Home'
import Manager from './Manager/Manager'
import Search from './Search/Search'
import AlertBox from './AlertBox/AlertBox'

// App is the main component and the 'single source of truth' for this application
// all configuration is done here(api url definition), it kick starts the app and observes state
class App extends React.Component {

  constructor() {
    super()
    this.state = {api: "http://localhost:8080/api"}
  }

  // display a navBar with a series of choices leading to different pages/components
  render() {
    const { api } = this.state
    
    return (
        <Router>
            <div id="App">
                <NavBar/>
                <Switch>
                <Route path="/manage/:entity" render={(props) => <Manager {...{...props, api: api}}/>}/>
                <Route path="/" exact component={Home}/>
                <Route path="/search" render={(props) => <Search {...{...props, api: api}}/>}/>
                </Switch>
            </div>
            <AlertBox {...this}/>
        </Router>)
  }
}

export default App

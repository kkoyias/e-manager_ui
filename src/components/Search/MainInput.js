import React from 'react'
import ReactDOM from 'react-dom'

import entities from '../configuration/entities'
import './search.css'

class MainInput extends React.Component{

    constructor(props){
        super(props)

        // get keys for each entity that is supposed to be in the search list
        const categories = Object.entries(entities).filter(entry => entry[1].isSearchItem).map(item => item[0])
        this.state = {
            categories: categories,
            maxSize: Math.max(...categories.map(c => c.length)),
            alert: null
        }
        this.onSubmit = this.onSubmit.bind(this)
    }

    // hide alert message when particular events are triggered
    componentDidMount(){
        const hideAlertOn = ['click'], node = ReactDOM.findDOMNode(this)
        const alertCloser = () => this.setState({alert : null})

        hideAlertOn.forEach(event => node.addEventListener(event, alertCloser))
    }

    // once a category is picked, make sure it is a predefined one and pass it up to the main component
    onSubmit(event){
        event.preventDefault()
        const input = document.querySelector("input"), { value } = input

        // clear input 
        input.value = ""
        input.placeholder = value

        // validate input, if valid create a form for this category
        if(this.state.categories.includes(value)){
            this.props.createForm(value)
            this.setState({alert: null})
        }
        else
            this.setState({alert: "Παρακαλώ επιλέξτε μια από τις διαθέσιμες επιλογές!"})
    }

    render(){    
        const { maxSize, categories, alert } = this.state
        const options = categories.map((c, i) => <option key={i} value={c}></option>)

        return(
            <form id="form-options" onSubmit={this.onSubmit}>
                <label htmlFor="options" id="search-prompt">{"Παρακαλώ επιλέξτε μία κατηγορία:"}</label>
                <input list="sections"
                    type="text" 
                    id="search-main-input"
                    size={maxSize + 1} 
                    maxLength={maxSize}
                    autoFocus
                    placeholder = {categories && categories.length > 0 && "π.χ " + categories[0]}
                    autoComplete={"off"}
                />
                <datalist id="sections">{options}</datalist>
                {alert && <h6 id="search-alert">{alert}</h6>}
            </form>)
        }
} 

export default MainInput
